<?php

/*
|--------------------------------------------------------------------------
| روابط الموقع
|--------------------------------------------------------------------------
| في هذا الملف يتم تسجيل تعريف جميع روابط التطبيق التي سيتمكن المستخدم من استخدامها
|
*/


/**
 * روابط تسجيل الدخول وتسجيل مستخدميين جدد واعادة تعيين كلمة المرور
 */
Auth::routes();

/**
 * رابط الصفحة الرئيسية
 */
Route::get('/', 'Dashboard\DashboardController@index')->name('dashboard.main');
/**
 * رابط لوحة التحكم للمستخدمين
 */
Route::get('dashboard', 'Dashboard\DashboardController@index');

/**
 * روابط البحث عن طالبة في لوحة تحكم الموظفيين
 *
 */
// رابط البحث عن طالبة
Route::get('user/search', 'Dashboard\Employee\UserSearchController@index')->name('search.index');
// رابط نتيجة البحث عن طالبة
Route::post('user/search', 'Dashboard\Employee\UserSearchController@search')->name('search.result');


/**
 * روابط المخالفات
 */
// رابط تسجيل مخالفة جديدة
Route::get('violation/{id}/create', 'Dashboard\Employee\ViolationController@create')->name('violation.create');
// رابط حفظ المخالفة
Route::post('violation/{id}/store', 'Dashboard\Employee\ViolationController@store')->name('violation.save');
// رابط مشاهدة قائمة مخالفات طالبة معينة
Route::get('student/{id}/violations', 'Dashboard\Employee\StudentViolationListController@index')->name('violation.list');


// رابط مشاهدة مخالفة الطالب
Route::get('my-violations', 'Dashboard\Student\ViolationController@index')->name('student.violation');
// رابط امشاهدة تفاصيل مخالفة للطالب
Route::get('my-violations/{id}/show', 'Dashboard\Student\ViolationController@show')->name('violation.details.view');
