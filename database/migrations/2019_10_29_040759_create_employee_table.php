<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Employees table schema
 */
class CreateEmployeeTable extends Migration
{
    /**
     * Run the migration for employee profile.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable(false);
            $table->string('employee_badge')->nullable(false);
            $table->timestamps();

            $table->primary('user_id');
            $table->unique('employee_badge');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
