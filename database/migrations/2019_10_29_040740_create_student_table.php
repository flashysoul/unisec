<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Stundents table schema
 */
class CreateStudentTable extends Migration
{
    /**
     * Run the migrations for student profile
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable(false);
            $table->string('student_number');
            $table->string('degree_major');
            $table->timestamps();

            $table->primary('user_id');
            $table->unique('student_number');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
