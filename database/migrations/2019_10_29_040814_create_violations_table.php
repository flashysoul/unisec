<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Violation table schema
 */
class CreateViolationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('student_number');
            $table->unsignedBigInteger('type_id');
            $table->date('violation_date');
            $table->text('details');
            $table->timestamps();

            $table->foreign('student_number')->references('student_number')->on('students');
            $table->foreign('type_id')->references('id')->on('violation_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('violations');
    }
}
