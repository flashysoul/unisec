<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ViolationType;
use Faker\Generator as Faker;

$factory->define(ViolationType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
