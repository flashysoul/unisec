<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_id = DB::table('users')->insertGetId([
            'full_name' => 'سارا احمد',
            'email' =>  'sara11@test.com',
            'password' => bcrypt('password'),
            'user_type' => 'student',
            'phone' => '0587237322',
            'dob' => Carbon::now()->subYears(20),
            'gender' => 'female',
            'nationality' => 'سعودية',
            'national_id' => '122132323',
            'country' => 'المملكة العربية السعودية',
            'region' => 'المنطقة الوسطى',
            'city' => 'الرياض',
            'street' => 'شارع الملك فهد',
            'zipcode' => '62345',
            'email_verified_at' => Carbon::now(),
        ]);

        DB::table('students')->insert([
            'user_id' => $user_id,
            'student_number' =>  '269473',
            'degree_major' => 'تقنية المعلومات'
        ]);

        $user_id = DB::table('users')->insertGetId([
            'full_name' => 'سارا احمد',
            'email' =>  'alaa@test.com',
            'password' => bcrypt('password'),
            'user_type' => 'student',
            'phone' => '0583272722',
            'dob' => Carbon::now()->subYears(20),
            'gender' => 'female',
            'nationality' => 'سعودية',
            'national_id' => '122132323',
            'country' => 'المملكة العربية السعودية',
            'region' => 'المنطقة الوسطى',
            'city' => 'الرياض',
            'street' => 'شارع الملك فهد',
            'zipcode' => '62345',
            'email_verified_at' => Carbon::now(),
        ]);

        DB::table('students')->insert([
            'user_id' => $user_id,
            'student_number' =>  '269472',
            'degree_major' => 'تقنية المعلومات'
        ]);


        $user_id = DB::table('users')->insertGetId([
            'full_name' => 'محمد ابراهيم',
            'email' =>  'ashoms0a@gmail.com',
            'password' => bcrypt('password'),
            'network_id' => 'ashoms0a',
            'user_type' => 'employee',
            'phone' => '0506333652',
            'dob' => Carbon::now()->subYears(20),
            'gender' => 'male',
            'nationality' => 'سعودي',
            'national_id' => '٣٢٣٢٣٢٣٢٢٣٢',
            'country' => 'المملكة العربية السعودية',
            'region' => 'المنطقة الوسطى',
            'city' => 'الرياض',
            'street' => 'شارع الملك فهد',
            'zipcode' => '٢٢١٣٣',
            'email_verified_at' => Carbon::now(),
        ]);

        DB::table('employees')->insert([
            'user_id' => $user_id,
            'employee_badge' =>  '27875',
        ]);
    }
}
