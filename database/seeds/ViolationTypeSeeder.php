<?php

use App\Models\ViolationType;
use Illuminate\Database\Seeder;

class ViolationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = factory(ViolationType::class)->create([
            'name' => 'سرعة',
        ]);
        $type = factory(ViolationType::class)->create([
            'name' => 'نوع ثاني',
        ]);
        $type = factory(ViolationType::class)->create([
            'name' => 'نوع ثالث',
        ]);
    }
}
