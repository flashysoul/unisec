<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserType;
use App\Violation;
use Illuminate\Auth\Access\HandlesAuthorization;

class ViolationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any violations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the violation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Violation  $violation
     * @return mixed
     */
    public function view(User $user, Violation $violation)
    {
        //
    }

    /**
     * Determine whether the user can create violations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->user_type == UserType::employee;
    }

    /**
     * Determine whether the user can update the violation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Violation  $violation
     * @return mixed
     */
    public function update(User $user, Violation $violation)
    {
        if ($user->user_type == UserType::student) {
            return false;
        } elseif ($user->user_type == UserType::employee) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the violation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Violation  $violation
     * @return mixed
     */
    public function delete(User $user, Violation $violation)
    {
        if ($user->user_type == UserType::student) {
            return false;
        } elseif ($user->user_type == UserType::employee) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the violation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Violation  $violation
     * @return mixed
     */
    public function restore(User $user, Violation $violation)
    {
        if ($user->user_type == UserType::student) {
            return false;
        } elseif ($user->user_type == UserType::employee) {
            return true;
        }
    }

    /**
     * Determine whether the user can permanently delete the violation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Violation  $violation
     * @return mixed
     */
    public function forceDelete(User $user, Violation $violation)
    {
        var_dump("checking user");
        var_dump($user->user_type);
        return $user->user_type == UserType::employee;
        // if ($user->user_type == UserType::student) {
        //     return false;
        // } elseif ($user->user_type == UserType::employee) {
        //     return true;
        // }
    }
}
