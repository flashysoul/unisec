<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Student Profile Model
 *
 * This model will take care of maintaining student profile
 * details in the systenm
 */
class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_number', 'user_id', 'degree_major'
    ];

    /**
     * Student profile belongs to a student record in the
     * users table
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A user may have one or more violations registered under
     * their profile
     *
     * @return void
     */
    public function violations()
    {
        return $this->hasMany(Violation::class, 'student_number');
    }
}
