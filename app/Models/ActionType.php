<?php

namespace App\Models;

/**
 * Action Type Enumration
 *
 * This class will be used in order to check for action types
 * throught the application instead of hardcoding the values
 *
 * This makes it easier in case the value needs to be changed
 * in the future as we just need to change it in one place
 * and call it a day :)
 */
class ActionType
{
    /**
     * Create action
     */
    const create = "create";

    /**
     * Update action
     */
    const edit = "update";

    /**
     * Delete action
     */
    const delete = "delete";
}
