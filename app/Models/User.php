<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * User Model
 *
 * This model will take care of maintaining user details
 * in the system
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'password', 'network_id', 'phone', 'dob', 'gender',
        'nationality', 'nationality_id', 'country', 'region', 'city', 'street',
        'zipcode', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A user might have a student profile if they are
     * defined as a student in the system
     *
     * @return void
     */
    public function student()
    {
        return $this->hasOne(Student::class);
    }

    /**
     * A user might have an employee profile if they are
     * defined as such
     *
     * @return void
     */
    public function employee()
    {
        return $this->hasOne(Employee::class);
    }
}
