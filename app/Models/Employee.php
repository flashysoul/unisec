<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Employee Profile Details
 *
 * This model will take care of maintaining employee
 * profile details in the application
 */
class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_badge', 'user_id',
    ];

    /**
     * employee profile belongs to an employee record in the
     * users table
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
