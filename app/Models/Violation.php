<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Violation Model
 *
 * Taking care of maintaining violation details in the system
 */
class Violation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_number', 'type_id', 'details', 'violation_date'
    ];
    /**
     * A violation belongs to a user and in our case
     * they are issued against students
     *
     * @return void
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_number', 'student_number');
    }

    /**
     * A violation belongs to a type
     *
     * @return void
     */
    public function type()
    {
        return $this->belongsTo(ViolationType::class);
    }
}
