<?php

namespace App\Models;

/**
 * User Type Enumration
 *
 * This class will be used in order to check for user types
 * throught the application instead of hardcoding the values
 *
 * This makes it easier in case the value needs to be changed
 * in the future as we just need to change it in one place
 * and call it a day :)
 */
class UserType
{
    /**
     * Student User Type
     */
    const student = "student";

    /**
     * Employee User Type
     */
    const employee = "employee";
}
