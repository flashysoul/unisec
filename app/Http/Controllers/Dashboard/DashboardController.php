<?php

namespace App\Http\Controllers\Dashboard;

use App\Repositories\UserRepository;
use App\Models\UserType;
use App\Http\Controllers\Controller;

/**
 * Dashboard Controller
 *
 */
class DashboardController extends Controller
{
    /**
     * User Repository
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Dashboard page will be accessible only by
     * authentication users
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
    }

    /**
     * Dashboard main page
     *
     * @return view
     */
    public function index()
    {
        // Check if the user is already logged in or not and show the main page accordingly
        $user = $this->userRepository->getCurrentLoggedInUser();
        // Based on the user type, we will show the prorper dashboard
        switch ($user->user_type) {
            case UserType::student:
                return view('dashboard.student.menu.index');
                break;
            case UserType::employee:
                return redirect()->route('search.index');
                break;
            default:
                return view('login');
        }
    }
}
