<?php

namespace App\Http\Controllers\Dashboard\Employee;

use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Repositories\StudentRepository;
use App\Repositories\ViolationRepository;
use App\Models\ViolationType;
use App\Http\Controllers\Controller;
use App\Models\ActionType;
use App\Repositories\UserRepository;

/**
 * مخالفات الطلاب  *
 */
class ViolationController extends Controller
{

    /**
     * User Repository
     *
     * @var StudentRepository
     */
    private $studentRepository;

    /**
     * Violation Repository
     *
     * @var ViolationRepository
     */
    private $violationRepository;

    /**
     * User Repository
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Class constructor
     */
    public function __construct(StudentRepository $studentRepository, ViolationRepository $violationRepository, UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->studentRepository = $studentRepository;
        $this->violationRepository = $violationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Shows create violation form to the logged in staff
     *
     * @return View
     */
    public function create($user_id)
    {
        // Check if the user can create a violation or not
        if ($this->userRepository->isUserAuthorizationForAction(ActionType::create) == false) {
            return;
        }

        // Show violation form
        $violation_types = ViolationType::all();
        $student = $this->studentRepository->getStudentDetailsByUserId($user_id);
        return view('dashboard.employee.violation.create', ['student' => $student, 'violation_types' => $violation_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, $user_id)
    {
        // Check if the user can store a violation or not
        if ($this->userRepository->isUserAuthorizationForAction(ActionType::create) == false) {
            return;
        }

        // Go ahead and save the violation detials
        $student = $this->studentRepository->getStudentDetailsByUserId($user_id);
        if ($this->violationRepository->create($student, $request)) {
            return redirect()->back()->with('message', __('main.The new violation has been saved successfully'));
        }
    }
}
