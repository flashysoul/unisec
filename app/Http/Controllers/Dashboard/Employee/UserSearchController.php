<?php

namespace App\Http\Controllers\Dashboard\Employee;

use App\Repositories\StudentRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * User Controller
 *
 */
class UserSearchController extends Controller
{
    private $userRepository;
    private $studentRepository;

    /**
     * Class Constrcutor
     */
    public function __construct(UserRepository $userRepository, StudentRepository $studentRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;
        $this->studentRepository = $studentRepository;
    }

    /**
     * Show the dashboard for admin users
     *
     * @return View
     */
    public function index()
    {
        $users = [];
        $user = $this->userRepository->getCurrentLoggedInUser();
        return view('dashboard.employee.search.result', ['users' => $users, 'user' => $user]);
    }

    /**
     * Get the details of the student by student number
     *
     * @param Request $request
     * @return view
     */
    public function search(Request $request)
    {
        $user = $this->userRepository->getCurrentLoggedInUser();
        $users = $this->studentRepository->getStudentDetailsByStudentNumber($request->input('student_number'));
        return view('dashboard.employee.search.result', ['users' => $users, 'user' => $user]);
    }
}
