<?php

namespace App\Http\Controllers\Dashboard\Employee;

use App\Repositories\ViolationRepository;
use App\Http\Controllers\Controller;

/**
 * Employee Violation Controller
 *
 * This controller is responsible for listing
 * the selected student violations list
 */
class StudentViolationListController extends Controller
{
    /**
     * Violation Repository
     *
     * @var ViolationRepository
     */
    private $violationRepository;

    /**
     * Class constructor
     *
     * @param ViolationRepository $violationRepository
     */
    public function __construct(ViolationRepository $violationRepository)
    {
        // Protect this page so that only admin can view it
        $this->middleware('auth');
        $this->violationRepository = $violationRepository;
    }

    /**
     * List selected student violations
     *
     * @param integer $user_id student id
     * @return void
     */
    public function index($user_id)
    {
        $violations = $this->violationRepository->getViolationsByUserId($user_id);
        return view('dashboard.employee.violation.student', ['violations' => $violations]);
    }
}
