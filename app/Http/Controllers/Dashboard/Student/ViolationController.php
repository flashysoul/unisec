<?php

namespace App\Http\Controllers\Dashboard\Student;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\ViolationRepository;
use App\Models\Violation;

/**
 * Student Violations Controller
 */
class ViolationController extends Controller
{
    /**
     * Violation Repository
     *
     * @var ViolationRepository
     */
    private $violationRepository;
    private $userRepository;

    /**
     * Class constrructor
     *
     * @param ViolationRepository $violationRepository
     */
    public function __construct(ViolationRepository $violationRepository, UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->violationRepository = $violationRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Show all violations list of the current
     * logged in user
     *
     * @return View
     */
    public function index()
    {
        $user = $this->userRepository->getLoggedInUserDetails();
        $violations = $this->violationRepository->getMyViolations($user->student->student_number);
        return view('dashboard.student.violation.list', ['user' => $user, 'violations' => $violations]);
    }


    /**
     * This will be called when the student clicks on view my violation
     * details. It will show the student the current selection violation
     * detials
     *
     * @param integer $id selected violation id
     *
     * @return View
     */
    public function show($id)
    {
        $user = $this->userRepository->getLoggedInUserDetails();
        $violationDetails = $this->violationRepository->getViolationDetailsById($id);
        return view('dashboard.student.violation.show', ['user' => $user, 'violation' => $violationDetails]);
    }
}
