<?php

namespace App\Repositories;

use App\Models\Student;
use App\Models\Violation;
use Illuminate\Http\Request;

/**
 * Violations Repository
 *
 */
class ViolationRepository
{

    /**
     * Get violations list by student number
     *
     * @param integer $student_number
     * @return void
     */
    public function getViolationsByStudentNumber($student_number)
    { }

    /**
     * Get violation by specified user id
     *
     * @param integer $user_id
     * @return void
     */
    public function getViolationsByUserId($user_id)
    {
        $violations = [];
        $student = Student::where('user_id', $user_id)->first();
        if ($student->student_number > 0) {
            $violations = Violation::with(['student'])->where('student_number', $student->student_number)->get();
        }
        return $violations;
    }

    /**
     * Create a new violation for the given student
     *
     * @param Student $student
     * @param Request $request
     * @return void
     */
    public function create($student, $request)
    {
        return Violation::create([
            'student_number' => $student->student->student_number,
            'type_id' => $request->input('violation_type'),
            'details' => $request->input('violation_details'),
            'violation_date' => $request->input('violation_date')
        ]);
    }


    public function getMyViolations($student_number)
    {
        $violations = [];
        if ($student_number > 0) {
            $violations = Violation::with(['type', 'student'])->where('student_number', $student_number)->get();
        }
        return $violations;
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function getViolationDetailsById($id)
    {
        return  $violation = Violation::with(['type', 'student'])->find($id);
    }
}
