<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Student;

/**
 * Student Repository
 */
class StudentRepository
{

    /**
     * Get student details by given user id
     *
     * @param integer $user_id
     * @return Student
     */
    public function getStudentDetailsByUserId($user_id)
    {
        return User::with(['student'])->find($user_id);
    }

    /**
     * Get student details by student id
     *
     * @param integer $student_number
     * @return Student
     */
    public function getStudentDetailsByStudentNumber($student_number)
    {
        return Student::with(['user'])->where('student_number', $student_number)->get();
        // User::with(['student'])->student()->where('student_number', $student_number)->get();
    }
}
