<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;


/**
 * User Repository
 *
 */
class UserRepository
{

    /**
     * Get current logged in user
     *
     * @return App\Model\User
     */
    public function getCurrentLoggedInUser()
    {
        return Auth::user();
    }

    /**
     * Get current logged in user ID
     *
     * @return integer
     */
    public function getCurrentLoggedInUserId()
    {
        return Auth::user()->id;
    }

    /**
     * Get current user details
     *
     * @return App\Mode\User
     */
    public function getLoggedInUserDetails()
    {
        return User::with(['student'])->find($this->getCurrentLoggedInUserId());
    }

    /**
     * Check if the given action can be performed
     * by the currently logged in user
     *
     * @param String $action
     * @return boolean
     */
    public function isUserAuthorizationForAction($action)
    {
        $user = $this->getCurrentLoggedInUser();
        return $user->cant($action, Violation::class);
    }
}
