<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'تم تغيير كلمة المرور بنجاح!',
    'sent' => 'تم ارسال رابط تغيير كلمة السر إلى البريد الإلكتروني الذي قمت بتحديده',
    'token' => 'رابط إعادة تعيين كلمة السر غيير صحيح',
    'user' => "لم نتمكن من ايجاد مستخدم بالايميل المحدد",
    'throttled' => 'الرجاء الانتظار قبل المحاولة مرة اخرى',

];
