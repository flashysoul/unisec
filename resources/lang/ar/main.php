<?php

/**
 * في هذا الملف يوجد جميع الترجمة التي تستخدم في الموقع
 */
return [
    'Login' => 'تسجيل الدخول',
    'E-Mail Address' => 'البريد الإلكتروني',
    'Password' => 'كلمة المرور',
    'Remember Me' => 'تذكرني',
    'Forgot Your Password?' => 'هل نسيت كلمة السر؟',
    'Register' => 'تسجيل',
    'Don\'t have an account?' => 'ليس لديك حساب؟',
    'Name' => 'الاسم',
    'Confirm Password' => 'تأكيد كلمة المرور',
    'Already have an account?' => 'لديك حساب مسبق؟',
    'Reset Password' => 'تغيير كلمة المرور',
    'Send Password Reset Link' => 'ارسال رابط التغيير',
    'Back to login' => 'الرجوع الى صفحة تسجيل الدخول',
    'All rights are reserved to University of Hail, 2019.' => 'جميع الحقوق محفوظة لجامعة حائل ، ٢٠١٩',

    'University of Hail' => 'موقع جامعة حائل',

    'Personal File' => 'الملف الشخصي',
    'Violations' => 'المخالفات',
    'Classes Schedule' => 'الجدول الدراسي',
    'My Violations List' => 'قائمة مخالفاتي',


    'Student Name' => 'اسم الطالبة',
    'Student Number' => 'الرقم الجامعي',
    'Violation Type' => 'نوع المخالفة',
    'Violation Date' => 'تاريخ المخالفة',
    'Violation Details' => 'تفاصيل المخالفة',
    'Save violation' => 'حفظ المخالفة',
    'View Violations' => 'مشاهدة المخالفات السابقة',
    'Actions' => 'الخيارات',
    'Logout' => 'تسجيل الخروج',


    'Main Information' => 'المعلومات الرئيسية',
    'Address Details' => ' تفاصيل العنوان',
    'Date of Birth' => 'تاريخ الميلاد',

    'Country' => 'البلد',
    'Region' => 'المنطقة',
    'City' => 'المدينة',
    'Street' => 'اسم الشارع',
    'Zip Code' => 'الرمز البريدي',
    'Nationality' => 'الجنسية',
    'National ID Number' => 'رقم الهوية',

    'Search for student by' => 'البحث عن الطالبة ',
    'Enter your keyword and hit the search button' => 'قم بإدخال رقم الطالبة الجامعي ثم اضغط على زر البحث',
    'Search' => 'بحث',
    'Search results' => 'نتيجة البحث',
    'Welcome' => 'مرحبا بك ',
    'Violations Details' => 'تفاصيل المخالفة',
    'Create Violation' => 'تسجيل مخالفة',
    'Select violation date' => 'قم بتحديد تاريخ المخالفة',
    'Go Back' => 'الرجوع إلى قائمة مخالفاتي',

    'Information Message' => 'حالة الطلب',
    'The new violation has been saved successfully' => 'تم حفظ المخالفة بنجاح',
    'Student Violations List' => 'قائمة مخالفات الطالبة',

    'Go to Main Page' => 'الرجوع للصفحة الرئيسية',


    'Questionner' => 'استبيان',
    'Personal Information' => 'المعلومات الشخصية',
    'Registeration' => 'التسجيل',
    'Student Record' => 'سجل الطالب',
    'Student Payment' => 'المكافأت المالية',
    'Student Major' => 'التخصص',
    'change_major' => 'تغيير التخصص',
    'Student Application' => 'معاملات الطالب',
    'Student Affair' => 'شؤون الطلاب',
    'Re-Housing' => 'اعادة التسكين',
    'Main Page' => 'الصفحة الرئيسية',
    'Student Violation' => 'مخالفات الطالب',

];
