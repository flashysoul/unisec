<!doctype html>
<html lang="{{ app()->getLocale() }}"
      @if( app()->getLocale()  == 'en')
      dir="ltr"
      @else
      dir="rtl"
    @endif
>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('main.University of Hail') }}</title>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Changa&display=swap" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css')}} " rel="stylesheet">
</head>
