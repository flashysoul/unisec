<div class="text-gray-500 bottom-0 block w-full mb-10 mt-10 text-center">
    {{ __('main.All rights are reserved to University of Hail, 2019.') }}
</div>
<!-- Scripts -->
<script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
