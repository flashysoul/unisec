@include('layouts.header')

<!--Container -->
<div class="mx-auto bg-grey-400 font-display">
    <!--Screen-->
    <div class="min-h-screen flex flex-col">
        <!--Header Section Starts Here-->
        @include('dashboard.common.top_navigation')
        <div class="flex flex-1">
        <!--Main-->
            <main class="bg-white-300 flex-1 p-3 overflow-hidden">
                <div class="flex flex-col">
                    <div id="app">
                        @yield('content')
                    </div>
                    <!-- /Stats Row Ends Here -->
                </div>
            </main>
            <!--/Main-->
        </div>
    </div>
</div>
@include('layouts.footer')
