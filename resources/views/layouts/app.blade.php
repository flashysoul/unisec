@include('layouts.header')
<body class="bg-gray-100 h-screen antialiased leading-none">
<div id="app">
    <div class="flex justify-center mb-10 mt-10">
        <img src="{{asset('images/logo/logo.png')}}" alt="university of hail" class="w-64">
    </div>
    @yield('content')
</div>
@include('layouts.footer')
