@extends('layouts.dashboard')
@section('content')
    <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 font-display text-xs ">
        <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
            <form action="{{route('search.result')}}" method="POST">
                @csrf
                <div class="p-4">
                    <span class="text-gray-700 text-base">{{__('main.Search for student by')}}</span>
                    {{-- <div class="mt-2">
                    <label class="inline-flex items-center">
                        <input type="radio" class="form-radio" name="search_type" value="student_name">
                        <span class="mr-2">{{__('main.Student Name')}}</span>
                    </label>

                    <label class="inline-flex items-center mr-6">
                        <input type="radio" class="form-radio" name="search_type" value="student_number">
                        <span class="mr-2">{{__('main.Student Number')}}</span>
                    </label>
                </div> --}}
                    <label class="block mb-2">
                        <span class="text-gray-800">{{__('main.Enter your keyword and hit the search button')}}</span>
                        <input type="text" name="student_number" class="form-input mt-1 block w-1/2">
                    </label>
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4">
                        {{__('main.Search')}}
                    </button>
                </div>
            </form>
        </div>
    </div>


    <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 font-display text-xs">
        <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
            <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                {{ __('main.Search results') }}
            </div>
            <div class="p-3">
                <table class="table-responsive w-full rounded">
                    <thead class="bg-gray-700 text-white">
                    <tr>
                        <th class="border w-1/3 px-4 py-2">{{__('main.Student Name')}}</th>
                        <th class="border w-1/3 px-4 py-2">{{__('main.Student Number')}}</th>
                        {{-- <th class="border w-1/6 px-4 py-2">{{__('main.E-Mail Address')}}</th> --}}
                        <th class="border w-1/3 px-4 py-2">{{__('main.Actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $member)
                        <tr>
                            <td class="border px-4 py-2">{{$member->user->full_name}}</td>
                            <td class="border px-4 py-2">{{$member->student_number}}</td>
                            {{-- <td class="border px-4 py-2">{{$member->user->email}}</td> --}}


                            <td class="border px-4 py-4">

                                <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-2 mb-2"
                                   href="{{route('violation.create', ['id' => $member->user->id])}}">
                                    <i class="fas fa-add"></i>{{__('main.Create Violation')}}</a>
                                </a>

                                <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-2 mb-2"
                                   href="{{route('violation.list', ['id' => $member->user->id])}}">
                                    <i class="fas fa-add"></i>{{__('main.View Violations')}}</a>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/Grid Form-->
@endsection
