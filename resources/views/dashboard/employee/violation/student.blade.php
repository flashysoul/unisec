@extends('layouts.dashboard')
@section('content')
    <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 font-display text-xs">
        <div class="mb-2 w-full">
            <div class="px-2 py-3 text-xl text-gray-600">
                {{ __('main.Student Violations List') }}
            </div>
            <div class="p-3">
                <table class="table-responsive w-full rounded">
                    <thead class="bg-gray-700 text-white">
                    <tr>
                        <th class="border  px-4 py-2">{{__('main.Student Name')}}</th>
                        <th class="border  px-4 py-2">{{__('main.Student Number')}}</th>
                        <th class="border  px-4 py-2">{{__('main.Violation Type')}}</th>
                        <th class="border  px-4 py-2">{{__('main.Violation Date')}}</th>
                        {{-- <th class="border  px-4 py-2">{{__('main.Actions')}}</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($violations as $violation)
                        <tr>
                            <td class="border px-4 py-2">{{$violation->student->user->full_name}}</td>
                            <td class="border px-4 py-2">{{$violation->student_number}}</td>
                            <td class="border px-4 py-2">{{$violation->type->name}}</td>
                            <td class="border px-4 py-2">{{$violation->violation_date}} </td>

                            {{-- <td class="border px-4 py-4"> --}}
                            {{-- <a class="bg-blue-300 cursor-pointer rounded p-1 mx-1 inline-block text-white">
                                    <i class="fas fa-eye"></i></a>
                                </a> --}}
                            {{-- <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-2 mb-2"
                                    href="{{route('violation.details.view', ['id' => $violation->id])}}">
                            <i class="fas fa-add"></i>{{__('main.Violation Details')}}</a>
                            </a> --}}
                            {{-- </td> --}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
