@extends('layouts.dashboard')
@section('content')

    <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 font-display text-xs">
        <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
            @if(session('message'))
                <div class="px-2 py-3 border-solid border-gray-200 border-b">
                    <div class="bg-green-100 border-t border-b border-green-500 text-green-700 px-4 py-3" role="alert">
                        <p class="font-bold">{{ __('main.Information Message') }}</p>
                        <p class="text-sm">{{ session('message')}}</p>
                    </div>
                </div>
            @endif

            <div class="bg-gray-200 px-2 py-3 border-solid border-gray-200 border-b">
                {{ __('main.Violations Details') }}
            </div>
            <div class="p-3 w-full p-8">
                <form method="POST" action="{{ route('violation.save', ['id'=> $student->id]) }}">
                    @csrf
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                   for="grid-name">
                                {{ __('main.Student Name') }}
                            </label>
                            <input
                                class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white-500"
                                id="grid-name" name="student_full_name" type="text" disabled
                                value="{{$student->full_name}}">
                            {{-- <p class="text-red-500 text-xs italic">Please fill out this field.</p> --}}
                        </div>
                        <div class="w-full md:w-1/2 px-3">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                   for="grid-student-number">
                                {{ __('main.Student Number') }}
                            </label>
                            <input
                                class="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                id="grid-student-number" name="student_number" type="text" disabled
                                value="{{$student->student->student_number}}">
                        </div>
                    </div>


                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                   for="grid-violation-type">
                                {{ __('main.Violation Type') }}
                            </label>
                            <select id="grid-violation-type" name="violation_type" class="appearance-none block w-full border rounded py-3 px-4 mb-3 leading-tight focus:bg-white-500
                            focus:outline-none">
                                @foreach ($violation_types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                   for="grid-violation-date">
                                {{ __('main.Violation Date') }}
                            </label>
                            <input
                                class="appearance-none block w-full text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                id="grid-violation-date" name="violation_date" type="date">
                        </div>

                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6">
                        <div class="w-full md:w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                                   for="grid-violation-details">
                                {{ __('main.Violation Details') }}
                            </label>
                            <textarea
                                class="appearance-none block w-full text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                                id="grid-violation-details" name="violation_details" rows="15"></textarea>
                        </div>

                    </div>
                    <div class="flex flex-wrap -mx-3 mb-6 px-3 md:mb-0">

                        <button type="submit"
                                class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            {{__('main.Save violation')}}
                        </button>
                    </div>

            </div>
        </div>
    </div>
@endsection
