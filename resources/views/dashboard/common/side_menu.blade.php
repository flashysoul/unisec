<!--Sidebar-->
<aside id="sidebar" class="bg-gray-200 w-1/2 md:w-1/6 lg:w-1/6 border-r border-side-nav hidden md:block lg:block">
    <ul class="list-reset flex flex-col">
        @if(Auth::user()->user_type == 'student')
            <li class=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                <a href="{{route('dashboard.main')}}"
                   class="font-display font-hairline hover:font-normal text-sm text-nav-item no-underline">
                    <i class="fas fa-home float-right mx-2"></i>
                    {{__('main.Personal File') }}
                    <span><i class="fas fa-angle-left float-left"></i></span>
                </a>
            </li>

            <li class=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                <a href="{{route('student.violation')}}"
                   class="font-display font-hairline hover:font-normal text-sm text-nav-item no-underline">
                    <i class="fas fa-file-invoice-dollar float-right mx-2"></i>
                    {{ __('main.Violations') }}

                    <span><i class="fas fa-angle-left float-left"></i></span>
                </a>
            </li>
        @else
            <li class=" w-full h-full py-3 px-2 border-b border-light-border bg-white">
                <a href="{{route('dashboard.main')}}"
                   class="font-display font-hairline hover:font-normal text-sm text-nav-item no-underline">
                    <i class="fas fa-file-invoice-dollar float-right mx-2"></i>
                    {{ __('main.Violations') }}

                    <span><i class="fas fa-angle-left float-left"></i></span>
                </a>
            </li>
        @endif
        <li class=" w-full h-full py-3 px-2 border-b border-light-border bg-white">

            <a href="{{route('logout')}}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
               class="font-display font-hairline hover:font-normal text-sm text-nav-item no-underline">
                <i class="fas fa-lock float-right mx-2 align-baseline"></i>
                {{ __('main.Logout') }}

                <span><i class="fas fa-angle-left float-left"></i></span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</aside>
<!--/Sidebar-->
