<header class="bg-gray-700">
    <div class="flex justify-between">
        <div class="p-1 mx-3 inline-flex">
            <i class="fas fa-bars mt-1 p-3 text-white align-baseline xl:text-2xl" onclick="sidebarToggle()"></i>
            <img src="{{asset('images/logo/dashboard-logo.png')}}" alt="dashboard logo"/>
        </div>
        <div class="p-1 flex flex-row justify-center items-center">
            <a href="#" onclick="profileToggle()" class="text-white p-2 no-underline hidden md:block lg:block ml-10">
                {{ __('main.Welcome')}}<span class="text-blue-500">{{$user->full_name ?? ''}}</span></a>
        </div>
    </div>
</header>

