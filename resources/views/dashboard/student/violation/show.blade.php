@extends('layouts.student_dashboard')
@section('content')
    <div class="flex flex-1  flex-col md:flex-row lg:flex-row mx-2 font-display text-xs">
        <div class="mb-2 border-solid border-gray-300 rounded border shadow-sm w-full">
            <div class="bg-gray-700 text-white text-xl px-2 py-3 border-solid border-gray-200 border-b">
                {{ __('main.Violations Details') }}
            </div>
            <div class="p-3 w-full p-8">

                <div class="flex flex-wrap -mx-3 mb-6">
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1" for="grid-name">
                            {{ __('main.Student Name') }}
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white-500"
                            id="grid-name" name="student_full_name" type="text" disabled value="{{$user->full_name}}">
                        {{-- <p class="text-red-500 text-xs italic">Please fill out this field.</p> --}}
                    </div>
                    <div class="w-full md:w-1/2 px-3">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                               for="grid-student-number">
                            {{ __('main.Student Number') }}
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                            id="grid-student-number" name="student_number" type="text" disabled
                            value="{{$violation->student->student_number}}">
                    </div>
                </div>


                <div class="flex flex-wrap -mx-3 mb-6">
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                               for="grid-violation-type">
                            {{ __('main.Violation Type') }}
                        </label>
                        <input
                            class="appearance-none block w-full bg-gray-200 text-grey-darker border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                            id="grid-student-number" name="student_number" type="text" disabled
                            value="{{$violation->type->name}}">
                    </div>
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                               for="grid-violation-date">
                            {{ __('main.Violation Date') }}
                        </label>
                        <input
                            class="appearance-none block w-full text-grey-darker bg-gray-200  border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                            id="grid-violation-date" name="violation_date" type="date" disabled
                            value="{{ $violation->violation_date}}">
                    </div>

                </div>

                <div class="flex flex-wrap -mx-3 mb-6">
                    <div class="w-full md:w-full px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-light mb-1"
                               for="grid-violation-details">{{ __('main.Violation Details') }}
                        </label>
                        <textarea
                            class="appearance-none block w-full text-grey-darker border bg-gray-200 border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white-500 focus:border-gray-600"
                            id="grid-violation-details" name="violation_details" rows="15" disabled>{{$violation->details}}
                    </textarea>
                    </div>

                </div>
                <div class="flex flex-wrap -mx-3 mb-6 px-3 md:mb-0">

                    <a class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                       href="{{route('student.violation')}}">
                        {{__('main.Go Back')}}
                    </a>
                </div>

            </div>
        </div>
    </div>

@endsection
