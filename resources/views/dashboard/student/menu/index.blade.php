@extends('layouts.student_dashboard')

@section('content')
<!-- Three columns -->
<div class="flex flex-wrap mb-2 mt-20 pr-16 pl-16">
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/todo-list.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">{{__('main.Questionner')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4  h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/info.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Personal Information')}}</div>

            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/folder.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">{{__('main.Registeration')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/record.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">{{__('main.Student Record')}} </div>
            </div>
        </div>
    </div>

</div>
<div class="h-40"></div>
<div class="flex flex-wrap mb-2 pr-16 pl-16">
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/money.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Student Payment')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4  h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/major.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">{{__('main.Student Major')}}</div>

            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/change-major.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.change_major')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/file.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Student Application')}}</div>
            </div>
        </div>
    </div>

</div>


<div class="h-40"></div>
<div class="flex flex-wrap mb-2 pr-16 pl-16">
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/department.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Student Affair')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4  h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/recycle.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Re-Housing')}}</div>

            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/home.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2"> {{__('main.Main Page')}}</div>
            </div>
        </div>
    </div>
    <div class="w-1/4 h-12 flex-col justify-center text-center">
        <div class="max-w-sm rounded overflow-hidden shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/violation.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">
                    <a href="{{route('student.violation')}}"> {{__('main.Student Violation')}}</a>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="h-40"></div>
<div class="flex flex-wrap mb-2 pr-16 pl-16">
    <div class="w-1/4 flex-col justify-center text-center">
        <div class="max-w-sm rounded shadow-lg">
            <img class="w-1/4 self-center m-auto" src="/images/logout.png" alt="Sunset in the mountains">
            <div class="px-6 py-4">
                <div class="font-bold text-xl mb-2">
                    <a <a href="{{route('logout')}}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{__('main.Logout')}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
